import Vue from "vue";
import VueRouter from "vue-router";
import Routes from "./routes";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import VueI18n from "vue-i18n";
import messages from "./i18n/";

Vue.use(VueRouter);
const router = new VueRouter({
  routes: Routes,
  mode: "history",
});

Vue.use(VueI18n);
const i18n = new VueI18n({
  locale: "fa",
  messages,
});
Vue.config.productionTip = false;

new Vue({
  vuetify,
  render: (h) => h(App),
  router,
  i18n,
}).$mount("#app");
